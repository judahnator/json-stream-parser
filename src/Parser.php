<?php

namespace judahnator\JsonParser;

use JsonStreamingParser\Listener\SubsetConsumerListener;

class Parser implements \ArrayAccess, \Iterator
{

    private $stream;
    private $jsonKeys = [];

    public function __construct(string $fileLocation)
    {
        $this->stream = fopen($fileLocation, 'r');
    }

    public function __destruct()
    {
        if ($this->stream) fclose($this->stream);
    }

    public function __get($name)
    {
        $foundData = null;
        rewind($this->stream);
        $parser = new \JsonStreamingParser\Parser(
            $this->stream,
            new class ($foundData, $name) extends SubsetConsumerListener
            {

                private $foundData;
                private $searchKey;

                public function __construct(&$foundData, string $searchKey)
                {
                    $this->foundData = &$foundData;
                    $this->searchKey = $searchKey;
                }

                /**
                 * @param mixed $data
                 * @return boolean if data was consumed and can be discarded
                 */
                protected function consume($data): bool
                {
                    if ($this->key === $this->searchKey && !$this->foundData) {
                        $this->foundData = $data;
                        return true;
                    }else if (is_array($data) && array_key_exists($this->searchKey, $data) && !$this->foundData) {
                        $this->foundData = $data[$this->searchKey];
                        return true;
                    }
                    return false;
                }
            }
        );
        $parser->parse();
        return $foundData;
    }

    public function keys() {
        $keys = [];
        rewind($this->stream);
        $parser = new \JsonStreamingParser\Parser(
            $this->stream,
            new class ($keys) extends SubsetConsumerListener
            {

                private $keys;

                public function __construct(array &$keys)
                {
                    $this->keys = &$keys;
                }

                /**
                 * @param mixed $data
                 * @return boolean if data was consumed and can be discarded
                 */
                protected function consume($data)
                {
                    if (count($this->keyValueStack) === 0 && is_array($data)) {
                        $this->keys = array_keys($data);
                        return true;
                    }
                    return false;
                }
            }
        );
        $parser->parse();
        return $keys;
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return !is_null($this->$offset);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value) {}

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset) {}

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        if (empty($this->jsonKeys)) {
            $this->jsonKeys = $this->keys();
        }
        return $this->{current($this->jsonKeys)};
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        if (empty($this->jsonKeys)) {
            $this->jsonKeys = $this->keys();
        }
        next($this->jsonKeys);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        if (empty($this->jsonKeys)) {
            $this->jsonKeys = $this->keys();
        }
        return current($this->jsonKeys);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        if (empty($this->jsonKeys)) {
            $this->jsonKeys = $this->keys();
        }
        return $this->offsetExists($this->key());
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        if (empty($this->jsonKeys)) {
            $this->jsonKeys = $this->keys();
        }
        reset($this->jsonKeys);
    }
}